﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DotNetVersionDetector
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            rtb1.IsReadOnly = true;
            rtb1.Document.LineHeight = 1;
        }

        private void getNetButton_Click(object sender, RoutedEventArgs e)
        {
            rtb1.Document.Blocks.Clear();

            try
            {
                var res = Funcs.GetDotNetInfoText();
                rtb1.AppendText(res);
            }
            catch (Exception ex)
            {
                rtb1.AppendText("Exception: " + ex.Message);
            }
        }

        private void getNetCoreButton_Click(object sender, RoutedEventArgs e)
        {
            rtb1.Document.Blocks.Clear();

            try
            {
                var cmd = new Cmd();
                cmd.GetCmd("dotnet --list-runtimes");
                var res = cmd.StandardOutput + Environment.NewLine + cmd.StandardError;
                rtb1.AppendText(res);
            }
            catch (Exception ex)
            {
                rtb1.AppendText("Exception: " + ex.Message);
            }
        }

        private void getRuntimeInformationButton_Click(object sender, RoutedEventArgs e)
        {
            rtb1.Document.Blocks.Clear();

            try
            {
                var runtimeInformationClass = Type.GetType("System.Runtime.InteropServices.RuntimeInformation");
                var frameworkDescriptionProperty = runtimeInformationClass?.GetTypeInfo().GetProperty("FrameworkDescription",
                    BindingFlags.Static | BindingFlags.Public);
                var res = frameworkDescriptionProperty?.GetValue(null)?.ToString();
                rtb1.AppendText(res);
            }
            catch (Exception ex)
            {
                rtb1.AppendText("Exception: " + ex.Message);
            }
        }

    }
}
