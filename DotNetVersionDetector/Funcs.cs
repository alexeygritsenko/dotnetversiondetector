﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetVersionDetector
{
    public class Funcs
    {
        public static string GetDotNetInfoText()
        {
            string text = "";
            var releaseKeyRes = DotNetVersion.FindReleaseKey();
            if (releaseKeyRes != null)
            {
                int releaseKey = (int)releaseKeyRes;
                if (releaseKey > 0)
                {
                    //Console.WriteLine(releaseKey);
                    var version = DotNetVersion.Find(releaseKey);
                    bool isNewerLast = DotNetVersion.IsNewerLastKnown(releaseKey);
                    if (!isNewerLast)
                        text += version + " (" + releaseKey + ")";
                    else
                        text += version + " or later (" + releaseKey + ")";
                }
            }

            return text;
        }
    }
}
