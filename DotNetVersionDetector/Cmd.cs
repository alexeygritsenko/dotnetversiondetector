﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace DotNetVersionDetector
{
    public class Cmd
    {
        public string StandardOutput { get; set; }
        public string StandardError { get; set; }

        public int GetCmd(string par, string dir = null)
        {
            var processInfo = new ProcessStartInfo("cmd.exe", "/c " + par); //c ~= exit
            //processInfo.Verb = "runas"; //в данном случае указываем, что процесс должен быть запущен с правами администратора
            if (!string.IsNullOrEmpty(dir))
                processInfo.WorkingDirectory = dir;
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            var process = Process.Start(processInfo);

            process.OutputDataReceived += Cmd_OutputDataReceived;
            process.BeginOutputReadLine();

            process.ErrorDataReceived += Cmd_ErrorDataReceived;
            process.BeginErrorReadLine();

            process.WaitForExit();

            int exitCode = process.ExitCode;
            process.Close();
            return exitCode;
        }

        private void Cmd_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            StandardError += e.Data + Environment.NewLine;
        }

        private void Cmd_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            StandardOutput += e.Data + Environment.NewLine;
        }

    }
}
